defmodule Mobilizon.Events.Categories do
  @moduledoc """
  Module that handles event categories
  """
  import Mobilizon.Web.Gettext

  @default "MEETING"

  @spec default :: String.t()
  def default do
    @default
  end

  @spec list :: [%{id: atom(), label: String.t()}]
  def list do
    build_in_categories() ++ extra_categories()
  end

  @spec get_category(String.t() | nil) :: String.t()
  def get_category(category) do
    if category in Enum.map(list(), &String.upcase(to_string(&1.id))) do
      category
    else
      default()
    end
  end

  defp build_in_categories do
    [
      %{
        id: :ausflug,
        label: gettext("Ausflug")
      },
      %{
        id: :ausstellung,
        label: gettext("Ausstellung")
      },
      %{
        id: :demonstration,
        label: gettext("Demonstration")
      },
      %{
        id: :einweihung,
        label: gettext("Einweihung")
      },
      %{
        id: :filmvorfuehrung,
        label: gettext("Filmvorführung")
      },
      %{
        id: :fussball,
        label: gettext("Fußball")
      },
      %{
        id: :gedenken,
        label: gettext("Gedenken")
      },
      %{
        id: :infostand,
        label: gettext("Infostand")
      },
      %{
        id: :kuenstlerisches,
        label: gettext("Künstlerisches")
      },
      %{
        id: :kneipe,
        label: gettext("Kneipe")
      },
      %{
        id: :konzert,
        label: gettext("Konzert")
      },
      %{
        id: :kuefa,
        label: gettext("KüFa")
      },
      %{
        id: :lesung,
        label: gettext("Lesung")
      },
      %{
        id: :party,
        label: gettext("Party")
      },
      %{
        id: :sport,
        label: gettext("Sport")
      },
      %{
        id: :theater,
        label: gettext("Theater")
      },
      %{
        id: :verhandlung,
        label: gettext("Verhandlung")
      },
      %{
        id: :workshop,
        label: gettext("Workshop")
      },
      # Legacy default value
      %{
        id: :meeting,
        label: gettext("Infoveranstaltung")
      },
    ]
  end

  @spec extra_categories :: [%{id: atom(), label: String.t()}]
  defp extra_categories do
    :mobilizon
    |> Application.get_env(:instance)
    |> Keyword.get(:extra_categories, [])
  end
end
